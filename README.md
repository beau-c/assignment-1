# Assignment 1

This repository contains a submission for the first assignment of a computer store.

## Instructions
Host the contents on a webserver and run the index.html file from a web browser that supports Javascript ES6.

Within the site, there are three components:
- Bank - Used for viewing balances and requesting loans.
- Work - Used for performing work, depositing to the bank and repaying loans.
- Store - Used for browsing and purchasing laptops.

## Screenshots
![screenshot of the webpage](https://i.ibb.co/Qn4x5FB/Screenshot.png)

## Credits
The free and open-source Metro 4 CSS library was used for styling the webpage (https://metroui.org.ua/).

## License
#### GNU GPL v3
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
