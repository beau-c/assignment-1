// Imports
import { Bank } from "./bank.js";
import { Work } from "./work.js";

// Variables
const API_URL = "https://noroff-komputer-store-api.herokuapp.com";
let laptopData = [];
let laptopSelectedId = undefined;

const euroFormatter = new Intl.NumberFormat('nl-NL', {
  style: 'currency',
  currency: 'EUR'
});

const txtBalance = document.getElementById("txt_balance");
const txtLoan = document.getElementById("txt_loan");
const rowLoan = document.getElementById("row_loan");
const txtPayBalance = document.getElementById("txt_payBalance");

const imgLaptop = document.getElementById("img_laptop");
const dropdownLaptops = document.getElementById("dropdown_laptops");
const listLaptopFeatures = document.getElementById("ul_laptopFeatures");
const txtLaptopTitle = document.getElementById("txt_laptopTitle");
const txtLaptopDescription = document.getElementById("txt_laptopDescription");
const txtLaptopPrice = document.getElementById("txt_laptopPrice");

const btnGetLoan = document.getElementById("btn_getLoan");
const btnBank = document.getElementById("btn_bank");
const btnWork = document.getElementById("btn_work");
const btnRepayLoan = document.getElementById("btn_repayLoan");
const btnLaptopBuy = document.getElementById("btn_laptopBuy");

/**
 * Function used to update the values of the HTML elements on the page
 * to ensure the current information is displayed to the user.
 */
function updateHTMLElements() {
  txtBalance.innerHTML = euroFormatter.format(Bank.balance);
  txtLoan.innerHTML = euroFormatter.format(Bank.loan);
  rowLoan.style.display = (Bank.hasLoan() ? "block" : "none");
  btnGetLoan.disabled = Bank.hasLoan();

  txtPayBalance.innerHTML = euroFormatter.format(Work.payBalance);
  btnBank.disabled = Work.payBalance <= 0;
}

/**
 * Function used to initialize a dropdown list
 * of laptops.
 */
function initLaptops() {
  for (let i = 0; i < laptopData.length; ++i) {
    // Populate the dropdown list.
    const option = document.createElement("option");
    option.value = laptopData[i].id;
    option.innerHTML = laptopData[i].title;
    dropdownLaptops.appendChild(option);

    if (i === 0) {
      // Select and display the first item by default.
      laptopSelectedId = laptopData[i].id;
      showLaptopDetails(laptopData[i]);
    }
  }
}

/**
 * Function used to display the details of a given laptop
 * by setting the corresponding HTML elements' values.
 */
function showLaptopDetails(laptop) {
  listLaptopFeatures.innerHTML = "";

  laptop.specs.forEach((feature, i) => {
    const item = document.createElement("li");
    item.innerHTML = feature;
    listLaptopFeatures.appendChild(item);
  });

  imgLaptop.src = `${API_URL}/${laptop.image}`;
  txtLaptopTitle.innerHTML = laptop.title;
  txtLaptopDescription.innerHTML = laptop.description;
  txtLaptopPrice.innerHTML = euroFormatter.format(laptop.price);
}

// Requesting a loan
btnGetLoan.addEventListener("click", () => {

  let inputLoan = parseFloat(window.prompt("Please enter the requested loan."));

  if (isNaN(inputLoan)) {
    window.alert("Invalid number.");
    return;
  }

  Bank.getLoan(inputLoan);
  updateHTMLElements();
});

// Depositing all funds from work to bank
btnBank.addEventListener("click", () => {
  Work.depositToBank();
  updateHTMLElements();
});

// Performing work
btnWork.addEventListener("click", () => {
  Work.performWork();
  updateHTMLElements();
});

// Repaying loan to the bank
btnRepayLoan.addEventListener("click", () => {
  Work.repayLoan();
  updateHTMLElements();
});

// Selecting a single laptop and displaying its details
dropdownLaptops.addEventListener("change", (e) => {
  let id = e.target.value;
  let laptop = laptopData.find(l => l.id == id);
  laptopSelectedId = laptop.id;
  showLaptopDetails(laptop);
});

// OnError event for image loading errors
imgLaptop.addEventListener("error", (e) => {
  imgLaptop.src = "images/image_not_found.png";
});

// Buying a selected laptop
btnLaptopBuy.addEventListener("click", () => {
  let laptop = laptopData.find(l => l.id == laptopSelectedId);

  try {
    Bank.withdraw(laptop.price);
    window.alert(`Congratulations! You are now the owner of the ${laptop.title}.`);
  }
  catch (e) {
    window.alert("Sorry! You don't have enough money to buy this laptop!");
  }

  updateHTMLElements();
});

// Fetch the laptop data
fetch(`${API_URL}/computers`)
  .then(response => response.json())
  .then(laptops => laptopData = laptops)
  .then(laptopData => initLaptops());

updateHTMLElements(); // Initially update the display of the HTML elements.
