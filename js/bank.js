export class Bank {

  static balance = 0;
  static loan = 0;

  // Indicates whether the account has a pending loan or not
  static hasLoan() {
    return Bank.loan > 0;
  }

  // Depositing a specified amount onto the balance
  static deposit(amount) {
    this.balance += amount;
  }

  // Withdrawing a specified amount from the balance
  static withdraw(amount) {
    if (this.balance - amount < 0) {
      throw new Error("Insufficient funds.");
    }

    this.balance -= amount;
  }

  // Requesting a new loan
  static getLoan(amount) {

    if (Bank.hasLoan()) {
      window.alert("You already have a pending loan!");
      return;
    }
    if (amount <= 0) {
      window.alert("Amount must be greater than 0.");
      return;
    }
    if (amount > Bank.balance * 2) {
      window.alert("Loan may not exceed more than double of your balance.");
      return;
    }
    Bank.loan = amount;
    Bank.balance += amount;
  }

  // Depositing to repay an existing loan
  static repayLoan(amount) {

    if (amount <= 0) {
      window.alert("Amount must be greater than 0.");
      return;
    }

    if (amount > this.loan) {
      // In case the given amount exceeds the loan,
      // we pay off the loan and deposit the remainder into the balance.
      let exceededAmount = amount - this.loan;
      amount -= exceededAmount;
      Bank.deposit(exceededAmount);
    }

    Bank.loan -= amount;
  }
}
