import { Bank } from "./bank.js";

export class Work {

  static payBalance = 0;

  // Transfer money from pay to bank balance.
  static depositToBank() {

    if (Bank.hasLoan()) {
      // If there's a loan, deduct 10% and send it to repay the loan
      let loanRepayment = Work.payBalance * 0.1;
      Work.payBalance -= loanRepayment;
      Bank.repayLoan(loanRepayment);
    }

    Bank.deposit(Work.payBalance);
    Work.payBalance = 0;
  }

  static performWork() {
    Work.payBalance += 100;
  }

  // Using the pay to repay a pending loan from the bank
  static repayLoan() {
    if (Bank.hasLoan()) {
      Bank.repayLoan(Work.payBalance);
      Work.payBalance = 0;
    }
    else {
      window.alert("You don't have any outgoing loans.");
    }
  }
}
